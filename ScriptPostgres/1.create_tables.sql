--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

-- Started on 2017-06-28 11:30:44

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2139 (class 1262 OID 12373)
-- Dependencies: 2138
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 2 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 24667)
-- Name: abonne; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE abonne (
    uti_id integer NOT NULL,
    ope_id integer NOT NULL
);


ALTER TABLE abonne OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 24585)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16393)
-- Name: msg_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE msg_id_seq
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE msg_id_seq OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 24649)
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE message (
    msg_id integer DEFAULT nextval('msg_id_seq'::regclass) NOT NULL,
    msg_date_envoi date,
    msg_num_destinataire character varying(25) NOT NULL,
    msg_contenu character varying(2000),
    msg_acquittement boolean NOT NULL,
    uti_id integer NOT NULL,
    ope_id integer NOT NULL,
    msg_statut character varying,
    msg_complement_information character varying
);


ALTER TABLE message OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 24657)
-- Name: operateur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE operateur (
    ope_id integer NOT NULL,
    ope_nom character varying(25) NOT NULL,
    ope_url_envoi_msg character varying NOT NULL,
    ope_url_acq_msg character varying(25),
    ope_autorisation_depassement boolean DEFAULT false NOT NULL,
    ope_acces_credit boolean DEFAULT false NOT NULL
);


ALTER TABLE operateur OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 24662)
-- Name: utilisateurapplicatif; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE utilisateurapplicatif (
    uti_id integer NOT NULL,
    uti_quota_ope integer NOT NULL,
    uti_nom character varying(25) NOT NULL,
    uti_login character varying(25) NOT NULL,
    uti_mdp character varying(25) NOT NULL,
    uti_url_acquittement character varying,
    uti_limite_ecart integer,
    uti_seuil_alerte_credit integer,
    uti_timeout_acquittement integer
);


ALTER TABLE utilisateurapplicatif OWNER TO postgres;


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 183
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 11, true);


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 182
-- Name: msg_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('msg_id_seq', 79, true);



--
-- TOC entry 2009 (class 2606 OID 24671)
-- Name: prk_constraint_abonne; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abonne
    ADD CONSTRAINT prk_constraint_abonne PRIMARY KEY (uti_id, ope_id);


--
-- TOC entry 2003 (class 2606 OID 24656)
-- Name: prk_constraint_message; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT prk_constraint_message PRIMARY KEY (msg_id);


--
-- TOC entry 2005 (class 2606 OID 24661)
-- Name: prk_constraint_operateur; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY operateur
    ADD CONSTRAINT prk_constraint_operateur PRIMARY KEY (ope_id);


--
-- TOC entry 2007 (class 2606 OID 24666)
-- Name: prk_constraint_utilisateurapplicatif; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY utilisateurapplicatif
    ADD CONSTRAINT prk_constraint_utilisateurapplicatif PRIMARY KEY (uti_id);


--
-- TOC entry 2013 (class 2606 OID 24687)
-- Name: fk_abonne_ope_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abonne
    ADD CONSTRAINT fk_abonne_ope_id FOREIGN KEY (ope_id) REFERENCES operateur(ope_id);


--
-- TOC entry 2012 (class 2606 OID 24682)
-- Name: fk_abonne_uti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY abonne
    ADD CONSTRAINT fk_abonne_uti_id FOREIGN KEY (uti_id) REFERENCES utilisateurapplicatif(uti_id);


--
-- TOC entry 2011 (class 2606 OID 24677)
-- Name: fk_message_ope_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_message_ope_id FOREIGN KEY (ope_id) REFERENCES operateur(ope_id);


--
-- TOC entry 2010 (class 2606 OID 24672)
-- Name: fk_message_uti_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_message_uti_id FOREIGN KEY (uti_id) REFERENCES utilisateurapplicatif(uti_id);


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-06-28 11:30:45

--
-- PostgreSQL database dump complete
--

