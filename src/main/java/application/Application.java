package application;

import application.model.Message;
import application.repository.MessageRepository;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class Application implements CommandLineRunner{

    @Autowired
    DataSource dataSource;

    @Autowired
    MessageRepository messageRepository;

    public  static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Transactional(readOnly = true)
    public void run(String... args) throws Exception {
        System.out.println("DATASOURCE = " + dataSource);
        
        System.out.println("\n****  findAll  ****");
        for (Message msg : messageRepository.findAll()) {
            System.out.println(msg);
        }
        
                System.out.println("\n**** findById ****");
        for (Message msg : messageRepository.findById(12)) {
            System.out.println(msg);
        }

        
    }
}
