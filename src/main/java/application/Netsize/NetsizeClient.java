package application.Netsize;

import application.model.Message;
import application.model.Operateur;
import application.model.UtilisateurApplicatif;
import application.repository.MessageRepository;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import netsize.wsdl.SendRequest;
import netsize.wsdl.SendResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Mohamed
 */
public class NetsizeClient extends WebServiceGatewaySupport {

    @Autowired
    MessageRepository msgRepo;

    /* Pour la nouvelle API de netsize (WSDL)  */
    public void verificationFormatSms(List<Message> list) {

        System.out.println("application.Netsize.NetsizeClient.verificationFormatSms()");
        for (Message sms : list) {
            if (sms.getMsgId() == null || sms.getMsgNumDestinataire() == null) {
                  System.out.println("application.Netsize.NetsizeClient.verificationFormatSms() if");
                  System.out.println("application.Netsize.NetsizeClient.verificationFormatSms() id : " + sms.getMsgId());
                sms.setMsg_statut("Message Non Valide");
            } else {
                  System.out.println("application.Netsize.NetsizeClient.verificationFormatSms() else");
                String numDestinataire = sms.getMsgNumDestinataire();
                if (numDestinataire.startsWith("0")) {
                    if (numDestinataire.startsWith("0033")) {
                        if (numDestinataire.length() != 13) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("0033", "33"));
                            sms.setMsg_statut("Message valide");
                        }
                    } else {
                        if (numDestinataire.length() != 10) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("0", "33"));
                            sms.setMsg_statut("Message valide");
                        }
                    }
                } else {
                    if (numDestinataire.startsWith("+33")) {
                        if (numDestinataire.length() != 12) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsg_statut("Message valide");
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("+", ""));
                        }
                    } else {
                        sms.setMsg_statut("Message non valide");
                    }
                }
            }
        }
    }

    /* Pour l'ancienne API de netsize  */
    public void verificationFormatSmsAncien(List<Message> list) {
        for (Message sms : list) {
            if (sms.getMsgId() == null || sms.getMsgNumDestinataire() == null) {
                sms.setMsg_statut("Message Non Valide");
            } else {
                String numDestinataire = sms.getMsgNumDestinataire();
                if (numDestinataire.startsWith("0")) {
                    if (numDestinataire.startsWith("0033")) {
                        if (numDestinataire.length() != 13) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("0033", "%2B33"));
                            sms.setMsg_statut("Message valide");
                        }
                    } else {
                        if (numDestinataire.length() != 10) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("0", "%2B33"));
                            sms.setMsg_statut("Message valide");
                        }
                    }
                } else {
                    if (numDestinataire.startsWith("+33")) {
                        if (numDestinataire.length() != 12) {
                            sms.setMsg_statut("Message non valide");
                        } else {
                            sms.setMsg_statut("Message valide");
                            sms.setMsgNumDestinataire(numDestinataire.replaceFirst("+", "%2B"));
                        }
                    } else {
                        sms.setMsg_statut("Message non valide");
                    }
                }
            }
        }
    }

    /**
     * Utile pour envoyer un sms via le web service
     *
     * @param list liste des message à envoyer. Chaque message contient le
     * contenu du sms, le numéro du destinataire il doit contenir le code pays
     * par exemple un portable en france : "33612345678"
     * @param ope L'opérateur avec lequel on desire envoyer les sms
     * @param user L'utilisateur
     */
    /* Pour la nouvelle API de netsize (WSDL)  */
    public void send(List<Message> list, Operateur ope, UtilisateurApplicatif user) {
        String uri = ope.getOpeUrlEnvoiMsg();

        for (Message msg : list) {
            if (msg.getMsg_statut().equals("Message valide")) {
                SendRequest request = new SendRequest();

                // id du sms
                request.setCorrelationId(msg.getMsgId().toString());
                // "#NULL" signifie qu'on envoie vide pour les String.
                request.setOriginatingAddress("#NULL#");
                // -1 signifie qu'on envoie vide pour les int.
                request.setOriginatorTON(-1);
                // Numéro de tel
                request.setDestinationAddress(msg.getMsgNumDestinataire());
                // corps du sms
                request.setUserData(msg.getMsgContenu());
                request.setUserDataHeader("#NULL#");
                request.setDCS(-1);
                request.setPID(-1);
                request.setRelativeValidityTime(-1);
                request.setDeliveryTime("#NULL#");

                request.setStatusReportFlags(0);
                request.setAccountName("#NULL#");
                request.setReferenceId("#NULL#");
                request.setServiceMetaData("#NULL#");
                request.setCampaignName("#NULL#");
                // Login sur le service
                request.setUsername(user.getUtiLogin());
                // mdp sur le service
                request.setPassword(user.getUtiMdp());

                SendResponse response = (SendResponse) getWebServiceTemplate().marshalSendAndReceive(uri, request, new SoapActionCallback(uri));
                msg.setMsg_complement_information(response.getResponseMessage());
            } else {
                msg.setMsg_complement_information("Le message n'a pas ete envoye car le numéro du destinataire est invalide");
            }

        }
    }

    /* Pour l'ancienne API de netsize  */
    public void sendAncien(List<Message> list, Operateur ope, UtilisateurApplicatif user) throws MalformedURLException, IOException {

        String url = ope.getOpeUrlEnvoiMsg();

        StringBuilder sb = new StringBuilder();
        sb.append("sEndpointName=NSGClientASP%5ENetsize&");
        sb.append("sExtensionName=AGSMSExt^Netsize&");
        sb.append("sLogin=".concat(user.getUtiLogin()));
        sb.append("&sPassword=".concat(user.getUtiMdp()).concat("&"));

        StringBuilder sbb;
        String urlParameters;
        URL obj;
        HttpURLConnection con;

        for (Message msg : list) {

            if (msg.getMsg_statut().equals("Message valide")) {

                sbb = new StringBuilder();
                sbb.append(sb.toString());
                sbb.append("sMessage=".concat(msg.getMsgContenu()));
                sbb.append("&sTarget=".concat(msg.getMsgNumDestinataire()));
                urlParameters = sbb.toString();

                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                // Send post request
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();

                BufferedReader in;
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                msg.setMsg_complement_information(response.toString());

                if (msg.getMsg_complement_information().startsWith("0")) {
                    msg.setMsg_statut("Message envoyé");
                } else {
                    msg.setMsg_statut("Message non envoyé");
                }

            } else {
                msg.setMsg_complement_information("Le message n'a pas ete envoye car le numéro du destinataire est invalide");
            }
            msg.setMsgDateEnvoi(Date.from(Instant.now()));
            msgRepo.save(msg);
        }
    }

}
