package application.Netsize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;


/**
 *
 * @author Mohamed
 */
@Configuration
public class NetsizeConfiguration {
    
  
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("netsize.wsdl");
		return marshaller;
	}

	@Bean
	public NetsizeClient netsizeClient(Jaxb2Marshaller marshaller) {
		NetsizeClient client = new NetsizeClient();
		client.setDefaultUri("http://europe.ipx.com/api/services2/SmsMessagingApi10?wsdl");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
             
	}
    
}
