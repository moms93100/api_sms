package application.controller;

import application.exception.FournisseurNonValideException;
import application.exception.ParamatresNonValideException;
import application.exception.UtilisateurNonValideException;
import application.model.Message;
import application.repository.MessageRepository;
import application.service.Service;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 *
 * @author Mohamed
 */
@RestController
@Configurable
@ApplicationScope
public class MessageController {

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    Service service;

    //Lister tous les messages
    @RequestMapping("/ListerMessage")
    public List<Message> liste() {
        return messageRepository.findAll();
    }

    //Lister par numéro de destinataire
    @RequestMapping("/ListerNumero")
    public List<Message> listerNumero(@Param("msg_num_destinataire") String msg_num_destinataire) {
        return messageRepository.findByNumdestinataire(msg_num_destinataire);
    }

    //Lister les messages par ID
    @RequestMapping("/ListerID")
    public List<Message> listerID(@Param("msgId") Integer id) {
        return messageRepository.findById(id);
    }

    //Insertion d'un message dans la BDD
    @RequestMapping("/InsertMessage")
    public void ajout(@Param("msg_num_destinataire") String msg_num_destinataire, @Param("msg_contenu") String msg_contenu, @Param("uti_id") Integer uti_id, @Param("ope_id") Integer ope_id, @Param("msg_statut") String msg_statut, @Param("msg_complement_information") String msg_complement_information) {
        messageRepository.save(msg_num_destinataire, msg_contenu, uti_id, ope_id, msg_statut, msg_complement_information);
    }

    //Envoi de SMS 
    @RequestMapping("/EnvoiSMS")
    public List<Message> envoiSms(@Param("msg_contenu") String msg_contenu, @Param("uti_id") Integer uti_id, @Param("uti_login") String uti_login, @Param("uti_mdp") String uti_mdp, @Param("uti_nom") String uti_nom, @Param("ope_id") Integer ope_id, @Param("ope_nom") String ope_nom, @Param("uti_url_acquittement") String uti_url_acquittement, @Param("msg_num_destinataire") String... msg_num_destinataire) throws IOException, ParamatresNonValideException, FournisseurNonValideException, UtilisateurNonValideException {
        return service.InitialisationSms(msg_contenu, uti_id, uti_login, uti_mdp, uti_nom, ope_id, ope_nom, uti_url_acquittement, msg_num_destinataire);
    }
}
