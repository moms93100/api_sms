package application.controller;

import application.model.Operateur;
import application.repository.OperateurRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mohamed
 */
@RestController
public class OperateurController {
    @Autowired
    OperateurRepository operateurRepository;
    
     //Lister tous les operateurs
    @RequestMapping("/ListerOperateur")
    public List<Operateur> liste(){
        return operateurRepository.findAll();
    }
}
