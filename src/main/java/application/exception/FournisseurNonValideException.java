/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.exception;

/**
 *
 * @author Mohamed
 */
public class FournisseurNonValideException extends Exception{
    
    public FournisseurNonValideException(){
        
    }
    
    public FournisseurNonValideException(String message){
        super(message);
    }
    
}
