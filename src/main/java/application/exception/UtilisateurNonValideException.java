/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.exception;

/**
 *
 * @author Mohamed
 */
public class UtilisateurNonValideException extends Exception{
    
    public UtilisateurNonValideException(){
        
    }
    
    public UtilisateurNonValideException(String message){
        super(message);
    }
    
}