package application.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Moms
 */
@Entity
@Table(name = "message")
@NamedQueries({
    @NamedQuery(name = "Message.findAll", query = "SELECT m FROM Message m"),
    @NamedQuery(name = "Message.findById", query = "SELECT m FROM Message m WHERE m.msgId = :msgId"),
    @NamedQuery(name = "Message.findByNumdestinataire", query = "SELECT m FROM Message m WHERE m.msg_num_destinataire = :msg_num_destinataire"),
    @NamedQuery(name = "Message.findByStatut", query = "SELECT m FROM Message m WHERE m.msg_statut = :msg_statut"),
    @NamedQuery(name = "Message.findByContenumessage", query = "SELECT m FROM Message m WHERE m.msg_contenu = :msg_contenu")})

public class Message implements Serializable {

    @Id
   // @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="msg_id_seq")
    @SequenceGenerator(name="msg_id_seq", sequenceName="msg_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "msg_id")
    private Integer msgId;
    @Column(name = "msg_date_envoi")
    @Temporal(TemporalType.DATE)
    private Date msg_date_envoi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 25)
    @Column(name = "msg_num_destinataire")
    private String msg_num_destinataire;
    @Size(max = 2147483647)
    @Column(name = "msg_contenu")
    private String msg_contenu;
    @Size(max = 2147483647)
    @Column(name = "msg_statut")
    private String msg_statut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "msg_acquittement")
    private boolean msgAcquittement;
    @Size(min = 0, max = 255)
    @Column(name = "msg_complement_information")
    private String msg_complement_information;
    @JoinColumn(name = "uti_id", referencedColumnName = "uti_id")
    @ManyToOne(optional = false)
    private UtilisateurApplicatif utilisateur;
    @JoinColumn(name = "ope_id", referencedColumnName = "ope_id")
    @ManyToOne(optional = false)
    private Operateur operateur;

    public Message() {
    }

    public Message(Integer msgId) {
        this.msgId = msgId;
    }

    public Message(Integer msgId, String msg_num_destinataire, boolean msgAcquittement) {
        this.msgId = msgId;
        this.msg_num_destinataire = msg_num_destinataire;
        this.msgAcquittement = msgAcquittement;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public Date getMsgDateEnvoi() {
        return msg_date_envoi;
    }

    public void setMsgDateEnvoi(Date msg_date_envoi) {
        this.msg_date_envoi = msg_date_envoi;
    }

    public String getMsgNumDestinataire() {
        return msg_num_destinataire;
    }

    public void setMsgNumDestinataire(String msg_num_destinataire) {
        this.msg_num_destinataire = msg_num_destinataire;
    }

    public String getMsgContenu() {
        return msg_contenu;
    }

    public void setMsgContenu(String msg_contenu) {
        this.msg_contenu = msg_contenu;
    }

    public boolean getMsgAcquittement() {
        return msgAcquittement;
    }

    public void setMsgAcquittement(boolean msgAcquittement) {
        this.msgAcquittement = msgAcquittement;
    }

    public Integer getUtiId() {
        return utilisateur.getUtiId();
    }

    public void setUtiId(UtilisateurApplicatif user) {
        this.utilisateur = user;
    }

    public String getMsg_statut() {
        return msg_statut;
    }

    public void setMsg_statut(String msg_statut) {
        this.msg_statut = msg_statut;
    }

    public String getMsg_complement_information() {
        return msg_complement_information;
    }

    public void setMsg_complement_information(String msg_complement_information) {
        this.msg_complement_information = msg_complement_information;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (msgId != null ? msgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Message)) {
            return false;
        }
        Message other = (Message) object;
        if ((this.msgId == null && other.msgId != null) || (this.msgId != null && !this.msgId.equals(other.msgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Message[ msgId=" + msgId + " ]";
    }

    public Integer getOpeId() {
        return operateur.getOpeId();
    }

    public void setOpeId(Operateur operateur) {
        this.operateur = operateur;
    }
}
