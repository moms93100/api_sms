package application.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mohamed
 */
@Entity
@Table(name = "operateur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operateur.findAll", query = "SELECT o FROM Operateur o")
    , @NamedQuery(name = "Operateur.findByOpeId", query = "SELECT o FROM Operateur o WHERE o.opeId = :opeId")
    , @NamedQuery(name = "Operateur.findByOpeNom", query = "SELECT o FROM Operateur o WHERE o.opeNom = :opeNom")
    , @NamedQuery(name = "Operateur.findByOpeUrlEnvoiMsg", query = "SELECT o FROM Operateur o WHERE o.opeUrlEnvoiMsg = :opeUrlEnvoiMsg")
    , @NamedQuery(name = "Operateur.findByOpeUrlAcqMsg", query = "SELECT o FROM Operateur o WHERE o.opeUrlAcqMsg = :opeUrlAcqMsg")
    , @NamedQuery(name = "Operateur.findUrlByOpeNom", query = "SELECT o.opeUrlEnvoiMsg FROM Operateur o WHERE o.opeNom =:opeNom")})

public class Operateur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ope_id")
    private Integer opeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ope_nom")
    private String opeNom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ope_url_envoi_msg")
    private String opeUrlEnvoiMsg;
    @Size(max = 255)
    @Column(name = "ope_url_acq_msg")
    private String opeUrlAcqMsg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ope_autorisation_depassement")
    private boolean opeAutorisationDepassement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ope_acces_credit")
    private boolean opeAccesCredit;
    @OneToMany(mappedBy = "operateur")
    private List<Message> messageList;
    @JoinTable(name = "abonne", joinColumns = {
        @JoinColumn(name = "ope_id", referencedColumnName = "ope_id")}, inverseJoinColumns = {
        @JoinColumn(name = "uti_id", referencedColumnName = "uti_id")})
    @ManyToMany
    private List<UtilisateurApplicatif> utilisateurapplicatifList;

    public Operateur() {
    }

    public Operateur(Integer opeId) {
        this.opeId = opeId;
    }

    public Operateur(Integer opeId, String opeNom, String opeUrlEnvoiMsg) {
        this.opeId = opeId;
        this.opeNom = opeNom;
        this.opeUrlEnvoiMsg = opeUrlEnvoiMsg;
    }

    public Integer getOpeId() {
        return opeId;
    }

    public void setOpeId(Integer opeId) {
        this.opeId = opeId;
    }

    public String getOpeNom() {
        return opeNom;
    }

    public void setOpeNom(String opeNom) {
        this.opeNom = opeNom;
    }

    public String getOpeUrlEnvoiMsg() {
        return opeUrlEnvoiMsg;
    }

    public void setOpeUrlEnvoiMsg(String opeUrlEnvoiMsg) {
        this.opeUrlEnvoiMsg = opeUrlEnvoiMsg;
    }

    public String getOpeUrlAcqMsg() {
        return opeUrlAcqMsg;
    }

    public void setOpeUrlAcqMsg(String opeUrlAcqMsg) {
        this.opeUrlAcqMsg = opeUrlAcqMsg;
    }

    @XmlTransient
    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    @XmlTransient
    public List<UtilisateurApplicatif> getUtilisateurapplicatifList() {
        return utilisateurapplicatifList;
    }

    public void setUtilisateurapplicatifList(List<UtilisateurApplicatif> utilisateurapplicatifList) {
        this.utilisateurapplicatifList = utilisateurapplicatifList;
    }

    public boolean isOpeAutorisationDepassement() {
        return opeAutorisationDepassement;
    }

    public void setOpeAutorisationDepassement(boolean opeAutorisationDepassement) {
        this.opeAutorisationDepassement = opeAutorisationDepassement;
    }

    public boolean isOpeAccesCredit() {
        return opeAccesCredit;
    }

    public void setOpeAccesCredit(boolean opeAccesCredit) {
        this.opeAccesCredit = opeAccesCredit;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (opeId != null ? opeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operateur)) {
            return false;
        }
        Operateur other = (Operateur) object;
        return ((this.opeId.equals(other.opeId)) && this.opeNom.equals(other.opeNom) /*&& this.opeUrlEnvoiMsg.equals(other.opeUrlEnvoiMsg)*/);
    }

    @Override
    public String toString() {
        return "application.model.Operateur[ opeId=" + opeId + " opeNom : " + opeNom + "]";
    }
    
}
