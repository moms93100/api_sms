package application.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mohamed
 */
@Entity
@Table(name = "utilisateurapplicatif")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UtilisateurApplicatif.findAll", query = "SELECT u FROM UtilisateurApplicatif u")
    , @NamedQuery(name = "UtilisateurApplicatif.findByUtiId", query = "SELECT u FROM UtilisateurApplicatif u WHERE u.utiId = :utiId")
    , @NamedQuery(name = "UtilisateurApplicatif.findByUtiQuotaOpe", query = "SELECT u FROM UtilisateurApplicatif u WHERE u.utiQuotaOpe = :utiQuotaOpe")
    , @NamedQuery(name = "UtilisateurApplicatif.findByUtiNom", query = "SELECT u FROM UtilisateurApplicatif u WHERE u.utiNom = :utiNom")
    , @NamedQuery(name = "UtilisateurApplicatif.findByUtiLogin", query = "SELECT u FROM UtilisateurApplicatif u WHERE u.utiLogin = :utiLogin")
    , @NamedQuery(name = "UtilisateurApplicatif.findByUtiMdp", query = "SELECT u FROM UtilisateurApplicatif u WHERE u.utiMdp = :utiMdp")})
public class UtilisateurApplicatif implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "uti_id")
    private Integer utiId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "uti_quota_ope")
    private int utiQuotaOpe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "uti_nom")
    private String utiNom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "uti_login")
    private String utiLogin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "uti_mdp")
    private String utiMdp;
    @Size(max = 2147483647)
    @Column(name = "uti_url_acquittement")
    private String utiUrlAcquittement;
    @Column(name = "uti_limite_ecart")
    private Integer utiLimiteEcart;
    @Column(name = "uti_seuil_alerte_credit")
    private Integer utiSeuilAlerteCredit;
    @Column(name = "uti_timeout_acquittement")
    private Integer utiTimeoutAcquittement;
    @ManyToMany(mappedBy = "utilisateurapplicatifList")
    private List<Operateur> operateurList;
    @OneToMany(mappedBy = "utilisateur")
    private List<Message> messageList;

    public UtilisateurApplicatif() {
    }

    public UtilisateurApplicatif(Integer utiId) {
        this.utiId = utiId;
    }

    public UtilisateurApplicatif(Integer utiId, int utiQuotaOpe, String utiNom, String utiLogin, String utiMdp) {
        this.utiId = utiId;
        this.utiQuotaOpe = utiQuotaOpe;
        this.utiNom = utiNom;
        this.utiLogin = utiLogin;
        this.utiMdp = utiMdp;
    }

    public Integer getUtiId() {
        return utiId;
    }

    public void setUtiId(Integer utiId) {
        this.utiId = utiId;
    }

    public int getUtiQuotaOpe() {
        return utiQuotaOpe;
    }

    public void setUtiQuotaOpe(int utiQuotaOpe) {
        this.utiQuotaOpe = utiQuotaOpe;
    }

    public String getUtiNom() {
        return utiNom;
    }

    public void setUtiNom(String utiNom) {
        this.utiNom = utiNom;
    }

    public String getUtiLogin() {
        return utiLogin;
    }

    public void setUtiLogin(String utiLogin) {
        this.utiLogin = utiLogin;
    }

    public String getUtiMdp() {
        return utiMdp;
    }

    public void setUtiMdp(String utiMdp) {
        this.utiMdp = utiMdp;
    }

    @XmlTransient
    public List<Operateur> getOperateurList() {
        return operateurList;
    }

    public void setOperateurList(List<Operateur> operateurList) {
        this.operateurList = operateurList;
    }

    @XmlTransient
    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
    
     public String getUtiUrlAcquittement() {
        return utiUrlAcquittement;
    }

    public void setUtiUrlAcquittement(String utiUrlAcquittement) {
        this.utiUrlAcquittement = utiUrlAcquittement;
    }

    public Integer getUtiLimiteEcart() {
        return utiLimiteEcart;
    }

    public void setUtiLimiteEcart(Integer utiLimiteEcart) {
        this.utiLimiteEcart = utiLimiteEcart;
    }

    public Integer getUtiSeuilAlerteCredit() {
        return utiSeuilAlerteCredit;
    }

    public void setUtiSeuilAlerteCredit(Integer utiSeuilAlerteCredit) {
        this.utiSeuilAlerteCredit = utiSeuilAlerteCredit;
    }

    public Integer getUtiTimeoutAcquittement() {
        return utiTimeoutAcquittement;
    }

    public void setUtiTimeoutAcquittement(Integer utiTimeoutAcquittement) {
        this.utiTimeoutAcquittement = utiTimeoutAcquittement;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (utiId != null ? utiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UtilisateurApplicatif)) {
            return false;
        }
        UtilisateurApplicatif other = (UtilisateurApplicatif) object;
        return (this.utiId.equals(other.utiId) && this.utiNom.equals(other.utiNom));
    }

    @Override
    public String toString() {
        return "application.model.Utilisateurapplicatif[ utiId=" + utiId + " ]";
    }

}
