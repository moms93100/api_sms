package application.repository;

import application.model.Message;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MessageRepository extends CrudRepository<Message, Long> {

    @Override
    //Lister tous les messages
    public List<Message> findAll();

    //Lister les messages par ID
    @Query("select m from Message m where m.msgId =:msgId")
    public List<Message> findById(@Param("msgId") Integer id);

    //Lister par numéro de destinataire
    @Query("select m from Message m where m.msg_num_destinataire like :msg_num_destinataire")
    public List<Message> findByNumdestinataire(@Param("msg_num_destinataire") String msg_num_destinataire);

    @Override
    public void deleteAll();

    //Insertion d'un message dans la BDD        
    @Query(value = "insert into Message (msg_date_envoi, msg_num_destinataire, msg_contenu, msg_acquittement, uti_id, ope_id, msg_statut, msg_complement_information) VALUES (:CURRENT_DATE ,:msg_num_destinataire, :msg_contenu, false, :uti_id, :ope_id, :msg_statut, :msg_complement_information )", nativeQuery = true)
    public void save(@Param("msg_num_destinataire") String msg_num_destinataire, @Param("msg_contenu") String msg_contenu, @Param("uti_id") int uti_id, @Param("ope_id") int ope_id, @Param("msg_statut") String msg_statut, @Param("msg_complement_information") String msg_complement_information);

}
