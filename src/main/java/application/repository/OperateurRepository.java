package application.repository;

import application.model.Operateur;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 *
 * @author Mohamed
 */

@RepositoryRestResource
public interface OperateurRepository extends CrudRepository<Operateur, Long> {
    
   
        //Lister tous les messages
        @Override
    	public List<Operateur> findAll();
        
        public Operateur findByOpeId(@Param("opeId")Integer id);
}
