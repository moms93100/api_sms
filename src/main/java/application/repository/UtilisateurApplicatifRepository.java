package application.repository;

import application.model.UtilisateurApplicatif;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 *
 * @author Mohamed
 */

@RepositoryRestResource
public interface UtilisateurApplicatifRepository extends CrudRepository<UtilisateurApplicatif, Long> {
    
}