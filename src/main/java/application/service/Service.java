package application.service;

import application.Netsize.NetsizeClient;
import application.exception.FournisseurNonValideException;
import application.exception.ParamatresNonValideException;
import application.exception.UtilisateurNonValideException;
import application.model.Message;
import application.model.Operateur;
import application.model.UtilisateurApplicatif;
import application.repository.MessageRepository;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author Mohamed
 */
@org.springframework.stereotype.Service
public class Service extends WebServiceGatewaySupport {

    @Autowired
    EntityManager em;

    @Autowired
    NetsizeClient netsizeClient;

    @Autowired
    MessageRepository messageRepository;

    public void erreur(List<Message> list, String message) {
        for (Message msg : list) {
            msg.setMsg_statut("Message non envoyé");
            msg.setMsg_complement_information(message);
        }
    }

    public boolean verificationParametres(List<Message> list, Operateur ope, UtilisateurApplicatif user) {

        if (list == null || ope == null || user == null) {
            erreur(list, "Au moins un des paramtres est null");
            return false;
        } else {
            if (ope.getOpeId() == null || ope.getOpeNom() == null) {
                erreur(list, "L'Opérateur est invalide");
                return false;
            } else {
                if (user.getUtiId() == null || user.getUtiNom() == null || user.getUtiLogin() == null || user.getUtiMdp() == null) {
                    erreur(list, "L'Utilisateur est invalide");
                    return false;
                }
            }
        }

        return true;
    }

    public boolean joignable(Operateur ope) throws MalformedURLException, IOException {

        //Mode bouchon
        if (ope.getOpeId() == 0) {
            return true;
        }
        URL obj = new URL(ope.getOpeUrlEnvoiMsg());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        int responseCode = 500;

        try {
            responseCode = con.getResponseCode();
        } catch (IOException e) {
        }

        return !(Integer.toString(responseCode).startsWith("5"));

    }

    public void verificationFormatSms(List<Message> list, Operateur ope) {

        switch (ope.getOpeId()) {
            //Mode Bouchon
            case 0:
                break;
            //Netsize
            case 1:
                netsizeClient.verificationFormatSmsAncien(list);
                break;
            //Netsize WSDL
            case 2:
                netsizeClient.verificationFormatSms(list);
                break;

            default:
                erreur(list, "Opérateur inconnu");
                break;
        }
    }

    public boolean appelServiceEnvoi(List<Message> list, Operateur ope, UtilisateurApplicatif user) throws MalformedURLException, IOException {

        switch (ope.getOpeId()) {

            //Mode bouchon
            case 0:
                for (Message msg : list) {
                    msg.setMsg_statut("Message envoyé");
                    msg.setMsg_complement_information("OK");
                }
                return true;

            //Netsize ancienne version api
            case 1:
                netsizeClient.sendAncien(list, ope, user);
                return true;

            //Netsize WSDL
            case 2:
                netsizeClient.send(list, ope, user);
                return true;
            default:
                erreur(list, "Opérateur inconnu");
                return false;
        }

    }

    public List<Message> InitialisationSms(String msg_contenu,
            Integer uti_id, String uti_login, String uti_mdp, String uti_nom,
            Integer ope_id, String ope_nom, String uti_url_acquittement, String... msg_num_destinataire) {

        Operateur ope = new Operateur(ope_id);
        UtilisateurApplicatif user = new UtilisateurApplicatif(uti_id);
        List<Message> list = new ArrayList<Message>();

        // S'il n'y a aucun numéro saisi ont crée un message d'erreur
        if (msg_num_destinataire == null) {
            Message erreur = new Message();
            erreur.setMsgNumDestinataire("***");
            erreur.setMsgContenu(msg_contenu);
            erreur.setOpeId(new Operateur(2));
            erreur.setUtiId(new UtilisateurApplicatif(1));
            list.add(erreur);
            erreur(list, "Aucun num destinataire saisi");
            return list;
        }

        for (String s : msg_num_destinataire) {
            Message msg = new Message(1);
            msg.setMsgDateEnvoi(Date.from(Instant.now()));
            msg.setMsgNumDestinataire(s);
            msg.setMsg_statut("Création message");
            msg.setMsgContenu(msg_contenu);
            msg.setUtiId(user);
            msg.setOpeId(ope);
            list.add(msg);
        }

        //Comparaison de l'operateur saisi avec celui en BDD
        Query query = em.createNamedQuery("Operateur.findByOpeId", Operateur.class);
        query.setParameter("opeId", ope_id);

        try {
            ope = (Operateur) query.getSingleResult();
            //Comparaison de l'operateur saisi avec celui en BDD
            if (!ope.getOpeNom().equals(ope_nom)) {
                erreur(list, "Opérateur non conforme avec la BDD");
                return list;
            }

        } catch (Exception e) {
            erreur(list, "Identifiant Opérateur inconnu");
            return list;
        }

        //Comparaison de l'utilisateur saisi avec celui en BDD
        Query query2 = em.createNamedQuery("UtilisateurApplicatif.findByUtiId", UtilisateurApplicatif.class);
        query2.setParameter("utiId", uti_id);

        try {
            user = (UtilisateurApplicatif) query2.getSingleResult();
            //Comparaison de l'utilisateur saisi avec celui en BDD
            if (!user.getUtiNom().equals(uti_nom)) {
                erreur(list, "Utilisateur non conforme avec la BDD");
                return list;
            } else {
                user.setUtiLogin(uti_login);
                user.setUtiMdp(uti_mdp);
                if (uti_url_acquittement != null) {
                    user.setUtiUrlAcquittement(uti_url_acquittement);
                } else {
                    user.setUtiUrlAcquittement("http://");
                }
            }

        } catch (Exception e) {
            erreur(list, "Identifiant Utilisateur inconnu");
            return list;
        }

        try {
            return envoiSms(list, ope, user);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParamatresNonValideException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FournisseurNonValideException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UtilisateurNonValideException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Message> envoiSms(List<Message> list, Operateur ope, UtilisateurApplicatif user) throws IOException, ParamatresNonValideException, FournisseurNonValideException, UtilisateurNonValideException {

        if (verificationParametres(list, ope, user)) {
            if (joignable(ope)) {
                verificationFormatSms(list, ope);
                appelServiceEnvoi(list, ope, user);
                /*for (Message msg : list) {
                    messageRepository.save(msg);
                }*/

            } else {
                erreur(list, "Opérateur non joignable");
            }
        }
        return list;
    }

}
