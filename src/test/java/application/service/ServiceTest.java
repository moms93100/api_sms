package application.service;

import application.model.Message;
import application.model.Operateur;
import application.model.UtilisateurApplicatif;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Mohamed
 */
//@AutoConfigureMockMvc
//@Component
//@RunWith(SpringRunner.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ServiceTest {

    @Autowired
    Service service;

    List<Message> list;
    Operateur ope;
    UtilisateurApplicatif user;

    public ServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        list = new ArrayList<Message>();
        ope = new Operateur();
        user = new UtilisateurApplicatif();

        Message msg1 = new Message(1);
        Message msg2 = new Message(2);
        Message msg3 = new Message(3);
        Message msg4 = new Message(4);
        Message msg5 = new Message(5);
        Message msg6 = new Message(6);

        msg1.setMsgNumDestinataire("0033667971049");//Message valide
        msg2.setMsgNumDestinataire("0667971049"); // Message valide
        msg3.setMsgNumDestinataire("003366745124521");// Message non valide
        msg4.setMsgNumDestinataire("066745124");//Message non valide
        msg5.setMsgNumDestinataire("66745124");//Message non valide
        msg6.setMsgNumDestinataire("");//Message non valide

        msg1.setMsg_statut("*");//Message valide
        msg2.setMsg_statut("*"); // Message valide
        msg3.setMsg_statut("*");// Message non valide
        msg4.setMsg_statut("*");//Message non valide
        msg5.setMsg_statut("*");//Message non valide
        msg6.setMsg_statut("*");//Message non valide

        msg1.setMsgContenu("Laurent, ceci est un test 1/2");//Message valide
        msg2.setMsgContenu("Ceci est la suite et fin 2/2"); // Message valide
        msg3.setMsgContenu("*");// Message non valide
        msg4.setMsgContenu("*");//Message non valide
        msg5.setMsgContenu("*");//Message non valide
        msg6.setMsgContenu("*");//Message non valide
        

/*
        // Netsize Ancien
        ope.setOpeId(1);
        ope.setOpeNom("Netsize");
        ope.setOpeUrlEnvoiMsg("http://fr.netsizeonline.com:8080/Request.aspx?");
        ope.setOpeUrlAcqMsg("http://fr.netsizeonline.com:8080/Request.aspx?");
        ope.setOpeAutorisationDepassement(false);
        ope.setOpeAccesCredit(false);
        ope.setMessageList(list);

        */
        
        // Netsize WSDL
        ope.setOpeId(2);
        ope.setOpeNom("Netsize");
        ope.setOpeUrlAcqMsg("http://fr.netsizeonline.com:8080/Request.aspx?");
        ope.setOpeAutorisationDepassement(false);
        ope.setOpeAccesCredit(false);
        ope.setMessageList(list);
        ope.setOpeUrlEnvoiMsg("http://europe.ipx.com/api/services2/SmsMessagingApi10?wsdl");
         
 /*
        // Bouchon
        ope.setOpeId(0);
        ope.setOpeNom("Bouchon");
        ope.setOpeUrlEnvoiMsg("http://fr.netsizeonline.com:8080/Request.aspx?");
         */
        user.setUtiId(1);
        user.setUtiLimiteEcart(10);
        //user.setUtiLogin("masocialesM-fr");
        //user.setUtiMdp("F7RHOF77");
        //user.setUtiLogin("minagriM-fr");
        //user.setUtiMdp("1DUDNB54");
        
        user.setUtiLogin("minagriM-fr");
        user.setUtiMdp("1DUDNB57");
        user.setUtiQuotaOpe(10);
        user.setUtiNom("SIAO");
        
        
        msg1.setOpeId(ope);
        msg2.setOpeId(ope);
        msg3.setOpeId(ope);
        msg4.setOpeId(ope);
        msg5.setOpeId(ope);
        msg6.setOpeId(ope);
        
        msg1.setUtiId(user);
        msg2.setUtiId(user);
        msg3.setUtiId(user);
        msg4.setUtiId(user);
        msg5.setUtiId(user);
        msg6.setUtiId(user);

        list.add(msg1);
        list.add(msg2);
        list.add(msg3);
        list.add(msg4);
        list.add(msg5);
        list.add(msg6);
        
        
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of verificationFormatSms method, of class Service.
     */
    @Test
    public void testVerificationFormatSms() {
      /*  System.out.println("verificationFormatSms");

        service.verificationFormatSms(list, ope);

        if (list.size() > 5) {

            for (int i = 0; i < 2; i++) {
               // assertEquals("Message valide", list.get(i).getMsg_statut());
               // assertEquals("%2B33667971049", list.get(i).getMsgNumDestinataire());
            }

            for (int i = 2; i < 6; i++) {
                //assertEquals("Message non valide", list.get(i).getMsg_statut());
            }
        } else {
            for (int i = 0; i < 2; i++) {
                //assertEquals("Message valide", list.get(i).getMsg_statut());
               // assertEquals("33667971049", list.get(i).getMsgNumDestinataire());
            }
        }
*/
    }

    /**
     * Test of envoiSms method, of class Service.
     */
    @Test
    public void testEnvoiSms() throws Exception {

        System.out.println("envoiSms");
        list = service.envoiSms(list, ope, user);

        for (Message msg : list) {
            System.out.println("Le statut du message : " + msg.getMsg_statut());
            System.out.println("Complement du message : " + msg.getMsg_complement_information());
        }
    }
}
